 **Spring Boot为基础，进行开发FastDFS的客户端，可独立运行的Jar文件。如果您有多个项目（组），并且拥有有文件服务器（FastDFS），那么为了节约其他项目同事的时间成本，这个访问文件服务器（FastDFS）的中间件绝对是个好选择。** 

启动修改端口命令：
    java -Dserver.port=8888 -jar fastDFS-Client.jar 192.168.1.157 22122

 - **福利** 

- spring boot 简单入门可以参考 
-     http://git.oschina.net/keeplearning996/springboot

- 接口使用文档
-     https://my.oschina.net/xwzj/blog/777136

- FastDFS的安装文档
-     https://my.oschina.net/xwzj/blog/760862

ps：差不多是两年前些的项目了，最近上来看到还有不少人在关注，所以找了个时间做了一下升级。也算给各位关注的朋友一个交代。谢谢大家